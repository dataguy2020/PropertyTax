# !/usr/bin/env python
author = "Michael Brown"

import pandas as pd
from limits import statelimit, annearundelcountylimit
from rates import statetaxrate, annearundeltaxrate, annearundelsolidwaste, annearundelstormwater
from warnings import simplefilter
from functions import taxcalculation, semiannualpayments, semiannualpayments1
import logging

simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

tanyardTH = pd.read_csv('test/7649.csv')

year1=tanyardTH['assessmentyear']
year2=year1+1
year3=year2+1

# year 1 calculation
tanyardTH['year1difference'] = tanyardTH['box8'] - tanyardTH['box4']
tanyardTH['year1countylimit'] = tanyardTH['box4'] + (tanyardTH['box4'] * annearundelcountylimit)
tanyardTH['year1statelimit'] = tanyardTH['box4'] + (tanyardTH['box4'] * statelimit)
tanyardTH['year1countydifference'] = tanyardTH['box8'] - tanyardTH['year1countylimit']
tanyardTH['year1statedifference'] = tanyardTH['box8'] - tanyardTH['year1statelimit']

# year1 county credit calculation
tanyardTH.loc[tanyardTH['year1countydifference'] < 0, 'year1countycredit'] = 0
tanyardTH.loc[tanyardTH['year1countydifference'] > 0, 'year1countycredit'] = (tanyardTH[
                                                                                  'year1countydifference'] * annearundeltaxrate) / 100

# year 1 state credit calculation
tanyardTH.loc[tanyardTH['year1statedifference'] < 0, 'year1statecredit'] = 0
tanyardTH.loc[tanyardTH['year1statedifference'] > 0, 'year1statecredit'] = (tanyardTH[
                                                                                'year1statedifference'] * statetaxrate) / 100
tanyardTH['year1totalcredit'] = tanyardTH['year1countycredit'] + tanyardTH['year1statecredit']

# year 1 straight real estate tax payment without exempt class
tanyardTH['year1countyrealestate'] = (tanyardTH['box4'] * annearundeltaxrate) / 100
tanyardTH['year1staterealestate'] = (tanyardTH['box4'] * statetaxrate) / 100
tanyardTH["year1total"] = tanyardTH.apply(
    lambda x: taxcalculation(x["owneroccupancycode"], x["homesteadcreditqualificationcode"], x["exemptclass"],
                             x["year1countyrealestate"], x["year1staterealestate"], x["year1countycredit"],
                             x["year1statecredit"]), axis=1)

tanyardTH["year1paytest"] = tanyardTH.apply(lambda x: semiannualpayments1(x["owneroccupancycode"], x["year1total"], x["county"]),
                                            axis=1)
tanyardTH["year1pay1"] = tanyardTH["year1paytest"][0][0]
tanyardTH["year1pay2"] = tanyardTH["year1paytest"][0][1]


# removing columns
tanyardTH.drop(['year1paytest'], axis=1, inplace=True)

# debugging
# print(tanyardTH.dtypes)
print(tanyardTH)
tanyardTH.to_csv('output/7649.csv')
